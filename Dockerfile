FROM centos
RUN yum update -y && yum upgrade -y && yum install deltarpm wget -y
RUN wget http://repos.1c-bitrix.ru/yum/bitrix-env.sh && chmod +x bitrix-env.sh 
# ./bitrix-env.sh [-s] [-p [-H hostname]] [-M mysql_root_password]
# -s - Тихий режим установки. Не задавать вопросы (Silent or quiet mode. Don't ask any questions).
# -p - Создать пул после установки окружения (Create pool after installation of bitrix-env).
# -H - Имя хоста (Hostname for for pool creation procedure).
# -M - Пароль root для MySQL (Mysql password for root user).
RUN ./bitrix-env.sh -s -p -H server1 -M '111111'
